// #define DEBUG
// #define DEBUGALL

#ifndef RC_INVOKED

#define MAX_ARGS 20

#define NUM_EXTENSIONS 2
static const char* exts[NUM_EXTENSIONS] = { ".exe", "" };

#ifndef __CYGWIN__
int WINAPI WinMain (HINSTANCE hSelf, HINSTANCE hPrev, LPSTR cmdline, int nShow);
static int realMain(int argc, char* argv[]);
#else
int main(int argc, char* argv[]);
static void add_path_cygwin(const char *str);
#endif

static int parse_args(int *argc, char* argv[]);
static int get_exec_name_and_path(char* execname, char* execpath);
static void xemacs_special(char* exec);
static void build_cmdline(char* new_cmdline, char* exec, int argc, char* argv[]);
static void process_execname(char *exec, const char* execname, const char* execpath);
static int file_exists_multi(char* fullname, const char* path,
                             const char* name_noext, const char* exts[],
                             const int extcnt);
static void add_path(const char *str);

#endif /* RC_INVOKED */

